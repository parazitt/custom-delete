def run():
    conf()
    # TODO improve this file


def conf():
    import configuration as c
    import connect
    import configparser
    print('Welcome!\nLet\'s  go ahead and config the bot\nNote: You can always modify this config by editing config.ini')
    if c.api_key:
        override = input('Looks like there\'s already a configuration done, do you wish to override it? (Y/N)')
        if override[0].lower() == 'n':
            return
    config = configparser.ConfigParser()
    s = 'Now please enter your API key, you can get it from http://t.me/BotFather\n'
    api_key = input(s)
    while True:
        bot = connect.connection('getMe', token=api_key)
        if not bot:
            api_key = input('Sorry, your token is invalid or a problem occurred, please try entering your key again:\n')
            continue
        break
    config['DEFAULT']['api_key'] = api_key
    config['BOT'] = {'username': bot['username'],
                     'id': bot['id'],
                     'name': bot['first_name']}
    with open('config.ini', 'w') as file:
        config.write(file)
    print('Bot information successfully updated.')

if __name__ == '__main__':
    run()
