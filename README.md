# TelegramBotClient
Palaton's Telegram bot client, uses requests to contact Telegram and loops in updates

# Installation
This bot is for Python 3.X only, No support is or will be for Python 2.X
1. Install requests package using pip

```shell
pip3 install requests
```
2. do the configuration via setup.py

```shell
python3 setup.py
```

# Usage
This bot can loop through updates in `main.py`, as well as you can use it with any standalone purpose.
you can use `run.sh` to run bot every 0.5 seconds.