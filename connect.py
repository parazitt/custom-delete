# Last update: May 23th 2017: Added file support
# Uses JSON and emoji to make the program fully Human-Machine readable
import json
import requests

import configuration as con

" This File is a connector file, it makes connection with Telegram API servers "
" Used python Requests additional library, it should be installed via pip (pip install requests) "


def connection(method, parameter={}, file=None, token=None):  # for files use {'name': binary_read}
    address = {'first': 'https://api.telegram.org/bot',
               'method': str(method),
               'token': con.api_key
               }
    # Program uses POST method of the library so parameters are sent by POST
    if token:
        address['token'] = token
    final_address = address['first'] + address['token'] + "/" + address['method']
    if not file:
        req = requests.post(final_address, parameter)
    else:
        req = requests.post(final_address, parameter, files=file)
    req_text = req.text
    req_final = json.loads(req_text)
    if req_final['ok']:  # This returns value only if request was correct
        return req_final['result']
    else:
        # TODO add a real logging system to this file
        print(req_final)
        return []
