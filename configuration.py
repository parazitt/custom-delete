import configparser
from os.path import isfile
config = configparser.ConfigParser()
if isfile('config.ini'):
    f = 'config.ini'
else:
    f = '../config.ini'
config.read(f)
api_key = config['DEFAULT']['api_key']
bot = {'id': config['BOT']['id'], 'username': config['BOT']['username'], 'name': config['BOT']['name']}
# database = {'host': config['DATABASE']['host'], 'username': config['DATABASE']['username'],
#            'password': config['DATABASE']['password']}
