""" This File is file that works with Connector file, Other files should not directly use connect"""
# Last update: June 30th 2017, Bot API v3.1: Admin tools update
# Last Function: unpinChatMessage
import connect as c


# Debugging functions
def out(text):
    from emoji import emojize
    return emojize(text, use_aliases=True)


def debug(text, name='HERE'):  # This function makes an output based on every function's arguments
    # like debug('chat_id = 2,d = False')
    # use it with make to get the name too
    # use like make('''getUpdates(offset=0, limit=100, timeout=0, allowed_updates=[])''')
    import re
    t = re.compile(r'''\s?([^,'"=]+)''', re.I)
    l = t.findall(text)  # List of every matching variable.
    txt = 'l={'  # Final text
    for i in l:
        invalids = ['None', 'False', 'True']
        if i in invalids:
            continue
        txt += "'{d}':{d}, ".format(d=i.strip())
    print('{}:'.format(name))
    print(txt[:len(txt) - 2] + "}")
    print('return c.connection(\'{n}\', l)'.format(n=name))


def make(text):
    import re
    t = re.compile(r'([a-z]+)\((.+)\):?(?s)', re.I)
    d = t.search(text)
    debug(d.group(2), d.group(1))


# Telegram Methods, updated until Bot API v3.0
def getMe():
    return c.connection("getMe")


# getUpdates
# Use this method to receive incoming updates using long polling. An Array of Update objects is returned.
def getUpdates(offset=0, limit=100, timeout=0, allowed_updates=''):
    l = {'offset': offset, 'limit': limit, 'timeout': timeout, 'allowed_updates': allowed_updates}
    return c.connection("getUpdates", l)


def sendMessage(chat_id, text, reply_to_message_id="", reply_markup="", parse_mode="Markdown",
                disable_web_page_preview=False, disable_notification=False):
    l = {'chat_id': chat_id, 'text': out(text), 'parse_mode': parse_mode,
         'disable_web_page_preview': disable_web_page_preview, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup, 'disable_notification': disable_notification
         }
    r = c.connection("sendMessage", l)
    return r


def forwardMessage(chat_id, from_chat_id, messaged_id, disable_notification=False):
    l = {'chat_id': chat_id, 'from_chat_id': from_chat_id, 'messaged_id': messaged_id,
         'disable_notification': disable_notification}
    return c.connection('forwardMessage', l)


# Sent content types
def sendPhoto(chat_id, photo, reply_markup="", caption="", disable_notification="", reply_to_message_id=""):
    l = {'chat_id': chat_id, 'photo': photo, 'reply_markup': reply_markup, 'caption': caption,
         'disable_notification': disable_notification, 'reply_to_message_id': reply_to_message_id}
    return c.connection("sendPhoto", l)


def sendAudio(chat_id, audio, caption='', duration='', performer='', title='', disable_notification=False,
              reply_to_message_id='', reply_markup=''):
    l = {'chat_id': chat_id, 'audio': audio, 'caption': caption, 'duration': duration, 'performer': performer,
         'title': title, 'disable_notification': disable_notification, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup}
    return c.connection('sendAudio', l)


def sendDocument(chat_id, document, reply_to_message_id='', reply_markup='', caption='', disable_notification=False):
    l = {'chat_id': chat_id, 'document': document, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup, 'caption': caption, 'disable_notification': disable_notification}
    return c.connection('sendDocument', l)


def sendSticker(chat_id, sticker, reply_markup="", disable_notification=False, reply_to_message_id=""):
    l = {'chat_id': chat_id, 'sticker': sticker, 'reply_markup': reply_markup,
         'disable_notification': disable_notification, 'reply_to_message_id': reply_to_message_id}
    return c.connection("sendSticker", l)


def sendVideo(chat_id, video, reply_to_message_id='', reply_markup='', duration='', width='', height='', caption='',
              disable_notification=False):
    l = {'chat_id': chat_id, 'video': video, 'reply_to_message_id': reply_to_message_id, 'reply_markup': reply_markup,
         'duration': duration, 'width': width, 'height': height, 'caption': caption,
         'disable_notification': disable_notification}
    return c.connection('sendVideo', l)


def sendVoice(chat_id, voice, reply_to_message_id='', reply_markup='', duration='', disable_notification=False):
    l = {'chat_id': chat_id, 'voice': voice, 'reply_to_message_id': reply_to_message_id, 'reply_markup': reply_markup,
         'duration': duration, 'disable_notification': disable_notification}
    return c.connection('sendVoice', l)


def sendVideoNote(chat_id, voice, reply_to_message_id='', reply_markup='', duration='', length='',
                  disable_notification=False):
    l = {'chat_id': chat_id, 'voice': voice, 'reply_to_message_id': reply_to_message_id, 'reply_markup': reply_markup,
         'duration': duration, 'length': length, 'disable_notification': disable_notification}
    return c.connection('sendVideoNote', l)


def sendLocation(chat_id, latitude, longitude, reply_to_message_id='', reply_markup='', disable_notification=False):
    l = {'chat_id': chat_id, 'latitude': latitude, 'longitude': longitude, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup, 'disable_notification': disable_notification}
    return c.connection('sendLocation', l)


def sendVenue(chat_id, latitude, longitude, reply_to_message_id='', reply_markup='', title='', address='',
              disable_notification=False):
    l = {'chat_id': chat_id, 'latitude': latitude, 'longitude': longitude, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup, 'title': title, 'address': address, 'disable_notification': disable_notification}
    return c.connection('sendVenue', l)


def sendContact(chat_id, phone_number, first_name, last_name='', reply_to_message_id='', reply_markup='',
                disable_notification=False):
    l = {'chat_id': chat_id, 'phone_number': phone_number, 'first_name': first_name, 'last_name': last_name,
         'reply_to_message_id': reply_to_message_id, 'reply_markup': reply_markup,
         'disable_notification': disable_notification}
    return c.connection('sendContact', l)


# Interactive, action can be typing for text messages, upload_photo for photos, record_video or upload_video for
# videos, record_audio or upload_audio for audio files, upload_document for general files, find_location for location
#  data, record_video_note or upload_video_note for video notes.
def sendChatAction(chat_id, action):
    l = {'chat_id': chat_id, 'action': action}
    return c.connection('sendChatAction', l)


# Returns an array of pictures
def getUserProfilePhotos(user_id, offset='', limit=''):
    l = {'user_id': user_id, 'offset': offset, 'limit': limit}
    return c.connection('getUserProfilePhotos', l)


# Use this method to get basic info about a file and prepare it for downloading. For the moment, bots can download
# files of up to 20MB in size. On success, a File object is returned. The file can then be downloaded via the link
# https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is taken from the response. It is
# guaranteed that the link will be valid for at least 1 hour.
def getFile(file_id):
    l = {'file_id': file_id}
    return c.connection('getFile', l)


def kickChatMember(chat_id, user_id, unti_date=''):
    l = {'chat_id': chat_id, 'user_id': user_id, 'until_date': unti_date}
    return c.connection('kickChatMember', l)


def unbanChatMember(chat_id, user_id):
    l = {'chat_id': chat_id, 'user_id': user_id}
    return c.connection('unbanChatMember', l)


def restrictChatMember(chat_id, user_id, until_date='', can_send_messages='', can_send_media_messages='',
                       can_send_other_messages='', can_add_web_page_previews=''):
    l = {'chat_id': chat_id, 'user_id': user_id, 'until_date': until_date, 'can_send_messages': can_send_messages,
         'can_send_media_messages': can_send_media_messages, 'can_send_other_messages': can_send_other_messages,
         'can_add_web_page_previews': can_add_web_page_previews}
    return c.connection('restrictChatMember', l)


def promoteChatMember(chat_id, user_id, can_change_info='', can_post_messages='', can_edit_messages='',
                      can_delete_messages='', can_invite_members='', can_restrict_members='', can_pin_messages='',
                      can_promote_members=''):
    l = {'chat_id': chat_id, 'user_id': user_id, 'can_change_info': can_change_info,
         'can_post_messages': can_post_messages, 'can_edit_messages': can_edit_messages,
         'can_delete_messages': can_delete_messages, 'can_invite_members': can_invite_members,
         'can_restrict_members': can_restrict_members, 'can_pin_messages': can_pin_messages,
         'can_promote_members': can_promote_members}
    return c.connection('promoteChatMember', l)


def exportChatInviteLink(chat_id):
    l = {'chat_id': chat_id}
    return c.connection('exportChatInviteLink', l)


def setChatPhoto(chat_id, photo):
    l = {'chat_id': chat_id, 'photo': photo}
    return c.connection('setChatPhoto', l)


def deleteChatPhoto(chat_id):
    l = {'chat_id': chat_id}
    return c.connection('deleteChatPhoto', l)


def setChatTitle(chat_id, title):
    l = {'chat_id': chat_id, 'title': title}
    return c.connection('setChatTitle', l)


def setChatDescription(chat_id, description):
    l = {'chat_id': chat_id, 'description': description}
    return c.connection('setChatDescription', l)


def pinChatMessage(chat_id, message_id, disable_notification=False):
    l = {'chat_id': chat_id, 'message_id': message_id, 'disable_notification': disable_notification}
    return c.connection('pinChatMessage', l)


def unpinChatMessage(chat_id):
    l = {'chat_id': chat_id}
    return c.connection('unpinChatMessage', l)


def leaveChat(chat_id):
    l = {'chat_id': chat_id}
    return c.connection("leaveChat", l)


# Functions used to update/receive info
def getChat(chat_id):
    l = {'chat_id': chat_id}
    return c.connection('getChat', l)


def getChatAdministrators(chat_id):
    l = {'chat_id': chat_id}
    return c.connection("getChatAdministrators", l)


def getChatMembersCount(chat_id):
    l = {'chat_id': chat_id}
    return c.connection('getChatMembersCount', l)


def getChatMember(chat_id, user_id):
    l = {'chat_id': chat_id, 'user_id': user_id}
    return c.connection('getChatMember', l)


# Callback answer
def answerCallbackQuery(callback_query_id, text, alert=False, url="", cache_time=0):
    l = {"callback_query_id": callback_query_id, 'text': text, "alert": alert, "url": url, "cache_time": cache_time}
    return c.connection("answerCallbackQuery", l)


# Message updating
def editMessageText(chat_id, message_id, text, reply_markup="", parse_mode="Markdown", disable_web_page_preview=False,
                    inline_message_id=""):
    l = {"chat_id": chat_id, "message_id": message_id, "text": text, "reply_markup": reply_markup,
         "parse_mode": parse_mode, "disable_web_page_preview": disable_web_page_preview,
         "inline_message_id": inline_message_id}
    return c.connection("editMessageText", l)


def editMessageCaption(chat_id='', message_id='', inline_message_id='', caption='', reply_markup=''):
    l = {'chat_id': chat_id, 'message_id': message_id, 'inline_message_id': inline_message_id, 'caption': caption,
         'reply_markup': reply_markup}
    return c.connection('editMessageCaption', l)


def editMessageReplyMarkup(reply_markup, chat_id='', message_id='', inline_message_id=''):
    l = {'reply_markup': reply_markup, 'chat_id': chat_id, 'message_id': message_id,
         'inline_message_id': inline_message_id}
    return c.connection('editMessageReplyMarkup', l)


def deleteMessage(chat_id, message_id):
    l = {'chat_id': chat_id, 'message_id': message_id}
    return c.connection('deleteMessage', l)


# Inline support added
def answerInlineQuery(inline_query_id, results, cache_time='', is_personal=True, next_offset='', switch_pm_text='',
                      switch_pm_parameter=''):
    l = {'inline_query_id': inline_query_id, 'results': results, 'cache_time': cache_time, 'is_personal': is_personal,
         'next_offset': next_offset, 'switch_pm_text': switch_pm_text, 'switch_pm_parameter': switch_pm_parameter}
    return c.connection('answerInlineQuery', l)


# Payments


def sendInvoice(chat_id, title, description, payload, provider_token, start_parameter, currency, prices, photo_url='',
                photo_size='', photo_width='', photo_height='', need_phone_number='', need_email='',
                need_shipping_address='', is_flexible='', disable_notification=False, reply_to_message_id='',
                reply_markup=''):
    l = {'chat_id': chat_id, 'title': title, 'description': description, 'payload': payload,
         'provider_token': provider_token, 'start_parameter': start_parameter, 'currency': currency, 'prices': prices,
         'photo_url': photo_url, 'photo_size': photo_size, 'photo_width': photo_width, 'photo_height': photo_height,
         'need_phone_number': need_phone_number, 'need_email': need_email,
         'need_shipping_address': need_shipping_address, 'is_flexible': is_flexible,
         'disable_notification': disable_notification, 'reply_to_message_id': reply_to_message_id,
         'reply_markup': reply_markup}
    return c.connection('sendInvoice', l)


def answerShippingQuery(shipping_query_id, ok, shipping_options='', error_message=''):
    l = {'shipping_query_id': shipping_query_id, 'ok': ok, 'shipping_options': shipping_options,
         'error_message': error_message}
    return c.connection('answerShippingQuery', l)


def answerPreCheckoutQuery(pre_checkout_query, ok, error_message=''):
    l = {'pre_checkout_query': pre_checkout_query, 'ok': ok, 'error_message': error_message}
    return c.connection('answerPreCheckoutQuery', l)


# Games
def sendGame(chat_id, game_short_name, disable_notification=False, reply_to_message_id='', reply_markup=''):
    l = {'chat_id': chat_id, 'game_short_name': game_short_name, 'disable_notification': disable_notification,
         'reply_to_message_id': reply_to_message_id, 'reply_markup': reply_markup}
    return c.connection('sendGame', l)


def setGameScore(user_id, score, force='', disable_edit_message='', chat_id='', message_id='', inline_message_id=''):
    l = {'user_id': user_id, 'score': score, 'force': force, 'disable_edit_message': disable_edit_message,
         'chat_id': chat_id, 'message_id': message_id, 'inline_message_id': inline_message_id}
    return c.connection('setGameScore', l)


def getGameHighScores(user_id, chat_id='', message_id='', inline_message_id=''):
    l = {'user_id': user_id, 'chat_id': chat_id, 'message_id': message_id, 'inline_message_id': inline_message_id}
    return c.connection('getGameHighScores', l)
