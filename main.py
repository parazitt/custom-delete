import telegram
import offset

def get():  # Starts
    last_update_id = offset.get()
    return telegram.getUpdates(last_update_id)


def single_type(in_single):
    l = ['edited_message', 'channel_post', 'edited_channel_post', 'inline_query', 'chosen_inline_result',
         'callback_query', 'message']
    for i in l:
        if i in in_single:
            return i

def message_types(single):
    types_list = ['audio', 'document', 'game', 'photo', 'sticker', 'video', 'voice', 'contact', 'location',
                  'venue', 'new_chat_member', 'left_chat_member', 'new_chat_title', 'new_chat_photo',
                  'delete_chat_photo', 'migrate_to_chat_id', 'migrate_from_chat_id', 'pinned_message',
                  'group_chat_created', 'video_note', 'invoice', 'successful_payment', 'text', 'audio']
    for i in types_list:
        if i in single:
            return i

def run():
    update = get()  # Gets last Update ID of the past session from File

    for single in update:  # Loops in each object of Update
        try:
            uid = int(single['update_id'])
            """ General Handlers """
            s_type = single_type(single)
            if s_type == 'message':
                single = single['message']
                admins = telegram.getChatAdministrators(single['chat']['id'])
                for admin in admins:
                    if admin['user']['id'] == single['from']['id']:
                        return
                if message_types(single) == 'document':
                    telegram.deleteMessage(single['chat']['id'], single['message_id'])
                elif 'entities' in single:
                    for ent in single['entities']:
                        if ent['type'] in ['url', 'text_link']:
                            telegram.deleteMessage(single['chat']['id'], single['message_id'])
        except Exception:
            continue
    if update:
        offset.update(uid + 1)


def debug_run():
    try:
        while True:
            run()
    except KeyboardInterrupt:
        from sys import exit
        exit()

if __name__ == '__main__':
    run()