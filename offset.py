# This module manages and stores update last offset in a json file, for real production, use MongoDB or MySQL instead.
import json
file_name = 'offset.json'


def get():
    try:
        with open(file_name) as file:
            d = file.read()
    except FileNotFoundError:
        return 0
    try:
        d = json.loads(d)
    except json.JSONDecodeError:
        return 0
    if 'update_id' not in d:
        return 0
    return d['update_id']


def update(new_id):
    with open(file_name, 'w') as file:
        file.write(json.dumps({'update_id': new_id}))

